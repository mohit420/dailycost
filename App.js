import React, { Component } from 'react';
import { AppRegistry, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { createDrawerNavigator, toggleDrawer } from 'react-navigation';

import History from './components/History';
import AddScreen from './components/AddScreen';

class Greeting extends Component {

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text onPress={() =>
                    navigate('AddScreen')}>DailyCost</Text>
            </View>
        );
    }
}

export default Home = createDrawerNavigator(
    {
        Home: { screen: Greeting },
        AddScreen: { screen: AddScreen },
        History: { screen: History },

    });

