import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, TextInput, Button, View, AsyncStorage } from 'react-native';

export default class store extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myKey: null
        }
    }

    async getKey() {
        try {
            const value = await AsyncStorage.getItem('@MySuperStore:key');
            this.setState({ myKey: value });
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }

    async saveKey(value) {
        try {
            await AsyncStorage.setItem('@MySuperStore:key', value);
        } catch (error) {
            console.log("Error saving data" + error);
        }
    }

    async resetKey() {
        try {
            await AsyncStorage.removeItem('@MySuperStore:key');
            const value = await AsyncStorage.getItem('@MySuperStore:key');
            this.setState({ myKey: value });
        } catch (error) {
            console.log("Error resetting data" + error);
        }

    }
}
AppRegistry.registerComponent('AsyncStorageExample', () => AsyncStorageExample);