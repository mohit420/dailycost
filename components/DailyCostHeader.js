import React, { Component } from 'react';
import { AppRegistry, View, styles02, Header, Ionicons, Text, ActivityIndicator, StyleSheet, Image, TouchableHighlight, Linking } from 'react-native';
//my import
import styles from '../components/styles'
import Icon from 'react-native-vector-icons/FontAwesome';

export default class DailyCostHeader extends Component {
    render() {

        return (
            <View style={styles.HeaderContainer}>
                <View style={styles.Headerrow}>
                    <Icon style={styles.menuicon}
                        name="align-justify"
                        size={20}
                    />
                    <Text style={styles.HeaderText}>
                        DAILY COST
                 </Text>
                    <Icon style={styles.searchIcon}
                        name="search"
                        size={20}
                    />
                </View>

            </View>
        );
    }
}