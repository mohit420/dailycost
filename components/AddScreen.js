import React, { Component } from 'react';
import { AppRegistry, AsyncStorage, View, Text, Button, TextInput, StyleSheet, Image, TouchableHighlight, Linking } from 'react-native';
import styles from '../components/styles';
import { createStackNavigator } from 'react-navigation';
import History from '../components/History';
import store from '../components/store';
import DailyCostHeader from '../components/DailyCostHeader';

export default class AddScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myKey: '',
            costKey: '',
            dateKey: '',
            descriptionKey: '',
            priceKey: '',
            text1: '',
            text2: '',
            text3: '',
            text4: '',

        }
    }

    async getKey() {
        try {
            //const value = await AsyncStorage.getItem('@MySuperStore:key');
            const key = await AsyncStorage.getItem('@MySuperStore:key');
            const key1 = await AsyncStorage.getItem('@MySuperStore:key1');
            const key2 = await AsyncStorage.getItem('@MySuperStore:key2');
            const key3 = await AsyncStorage.getItem('@MySuperStore:key3');
            const key4 = await AsyncStorage.getItem('@MySuperStore:key4');

            this.setState({
                myKey: key,
                dateKey: key1,
                costKey: key2,
                descriptionKey: key3,
                priceKey: key4
            });
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }

    async saveKey(text1, text2, text3, text4) {
        key = text1 + text2 + text3 + text4;
        try {
            await AsyncStorage.setItem('@MySuperStore:key', key);
            await AsyncStorage.setItem('@MySuperStore:key1', text1);
            await AsyncStorage.setItem('@MySuperStore:key2', text2);
            await AsyncStorage.setItem('@MySuperStore:key3', text3);
            await AsyncStorage.setItem('@MySuperStore:key4', text4);
        } catch (error) {
            console.log("Error saving data" + error);
        }
    }

    async resetKey() {
        try {
            await AsyncStorage.removeItem('@MySuperStore:key');
            await AsyncStorage.removeItem('@MySuperStore:key1');
            await AsyncStorage.removeItem('@MySuperStore:key2');
            await AsyncStorage.removeItem('@MySuperStore:key3');
            await AsyncStorage.removeItem('@MySuperStore:key4');

            const value = await AsyncStorage.getItem('@MySuperStore:key');
            const value1 = await AsyncStorage.getItem('@MySuperStore:key1');
            const value2 = await AsyncStorage.getItem('@MySuperStore:key2');
            const value3 = await AsyncStorage.getItem('@MySuperStore:key3');
            const value4 = await AsyncStorage.getItem('@MySuperStore:key4');

            this.setState({
                myKey: value,
                dateKey: value1,
                costKey: value2,
                descriptionKey: value3,
                priceKey: value4,
            });
        } catch (error) {
            console.log("Error resetting data" + error);
        }
    }
    ShowDate() {
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
        this.setState({
            getdate: date,
            getmonth: month,
            getyear: year,
        });
    }
    componentDidMount() {
        this.getKey();
        this.ShowDate();
    }

    render() {
        const { navigate } = this.props.navigation;
        const { newValue, height } = this.state;
        return (
            <View style={styles.MainContainer}>

                <DailyCostHeader />

                <View style={styles.ContaintStyles}>
                    <View style={styles.row}>
                        <Text style={styles.label}>Date</Text>
                        <Text>              {this.state.getdate}-{this.state.getmonth}-{this.state.getyear}</Text>
                    </View>

                    <View style={styles.row}>
                        <Text style={styles.label}>Cost Type</Text>
                        <TextInput style={styles.val}
                            placeholder="Enter Cost Type"
                            value={this.state.costKey}
                            onChangeText={(value) => this.setState({ text2: value })}
                            multiline={true}
                        />
                    </View>

                    <View style={styles.row}>
                        <Text style={styles.label}>Cost Description</Text>
                        <TextInput style={styles.val}
                            placeholder="Enter Cost Description"
                            value={this.state.descriptionKey}
                            onChangeText={(value) => this.setState({ text3: value })}
                            multiline={true}
                        />
                    </View>

                    <View style={styles.row}>
                        <Text style={styles.label}>Price</Text>
                        <TextInput style={styles.val}
                            placeholder="Enter Price"
                            value={this.state.priceKey}
                            onChangeText={(value) => this.setState({ text4: value })}
                            multiline={true}
                        />
                    </View>

                </View>
                <View style={styles.SaveButton}>
                    <Button
                        onPress={() => this.saveKey(this.state.text1, this.state.text2, this.state.text3, this.state.text4)}
                        title="Save"
                    />
                    <Button
                        style={styles.formButton}
                        onPress={this.resetKey.bind(this)}
                        title="Reset"
                        color="#f44336"
                        accessibilityLabel="Reset"
                    />
                </View>
                <Button
                    title='Add More'
                />
            </View>
        )
    }
}
