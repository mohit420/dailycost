import { StyleSheet, Dimensions } from 'react-native';

export default styles = StyleSheet.create({
    ///////////AddScreen.js

    MainContainer: {
        flex: 1,
        backgroundColor: 'yellow',
        paddingVertical: 24,
        justifyContent: 'space-between',
        flexDirection: 'column',
    },
    ContaintStyles: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 10,
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        //marginTop: ',
        width: '100%',
    },
    label: {
        height: 25,
        width: '40%',
        paddingLeft: 10,
        flexDirection: 'row',
    },
    val: {
        flex: 1,
        flexDirection: 'row',
        marginTop: -16,
        marginRight: 3,
        width: 150,
        height: 60,
        paddingLeft: 50
    },
    SaveButton: {
        //flex: 1,
        marginBottom: 150,
    },

    //////////////////DailyCostHeader.js

    HeaderContainer: {
        backgroundColor: 'green',
        height: 40,
        alignItems: 'center'
    },
    Headerrow: {
        flex: 1,
        marginTop: '0.5%',
        flexDirection: 'row',
        //alignItems: 'center'
    },
    HeaderText: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
    },
    searchIcon: {
        marginTop: '2%',
        color: "black",
        paddingLeft: 70,
        //marginLeft: 50,
    },
    menuicon: {
        marginTop: '2%',
        color: "black",
        paddingRight: 70,
    },

});
