import React, { Component } from 'react';
import AddScreen from '../components/AddScreen';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    Button,
    View,
    AsyncStorage
} from 'react-native';

export default class History extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myKey: ''
        }

    }
    render() {
        const key = this.props.navigation.getParam('key');
        const dateKey = this.props.navigation.getParam('dateKey');
        const costKey = this.props.navigation.getParam('costKey')
        const descriptionKey = this.props.navigation.getParam('descriptionKey')
        const priceKey = this.props.navigation.getParam('priceKey')

        return (
            <View style={styles.container}>
                <Text >
                    All key is = {key}
                </Text>
                <Text >
                    Date = {dateKey}
                </Text>
                <Text >
                    Cost Type = {costKey}
                </Text>
                <Text >
                    Cost Description = {descriptionKey}
                </Text>
                <Text >
                    Price = {priceKey}
                </Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        padding: 30,
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
});

